package ReportCard;

import java.util.HashMap;
import java.util.Scanner;

public class GradingApp {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student1.setName("Jane");
        student2.setName("Mary");
        student3.setName("John");

        student1.addGrade(85);
        student1.addGrade(87);
        student1.addGrade(89);

        student2.addGrade(95);
        student2.addGrade(97);
        student2.addGrade(99);

//        students.get("c").addGrade(84);
        student3.addGrade(84);
        student3.addGrade(86);
        student3.addGrade(88);

        System.out.println(student1.getName());
        System.out.println(student1.getGrades());
        System.out.println(student1.getGradeAverage());

        System.out.println(student2.getName());
        System.out.println(student2.getGrades());
        System.out.println(student2.getGradeAverage());

        System.out.println(student3.getName());
        System.out.println(student3.getGrades());
        System.out.println(student3.getGradeAverage());

        HashMap<String, String> students = new HashMap<>();

        students.put("janeclayton", student1.getName());
        students.put("marykringle", student2.getName());
        students.put("johnwick", student3.getName());

//        System.out.println(students);

        HashMap<String, Double> students2 = new HashMap<>();

        students2.put("janeclayton", student1.getGradeAverage());
        students2.put("marykringle", student2.getGradeAverage());
        students2.put("johnwick", student3.getGradeAverage());



        do {

            System.out.printf("%nWhich student would you like to see more information about?%n%n%s%n%n",students.keySet());

            String userInput = scanner.nextLine();

            if (students.get(userInput) == null) {
                System.out.printf("%nSorry no student found with the GitLab username: %s", userInput);
            }
            else {

                System.out.printf("%nName: %s - GitLab Username: %s%n", students.get(userInput), userInput);
                System.out.printf("%nCurrent average: %.2f%n", students2.get(userInput));
            }
//            System.out.println("\nWould you like to see another student?[y/n]");
//
//            userChoice = scanner.next();
//
//            System.out.println(userChoice);


        } while (true);

//        System.out.println("Goodbye, and have a wonderful day!");



    } // end of psvm

} // end of class
