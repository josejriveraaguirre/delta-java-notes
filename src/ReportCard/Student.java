package ReportCard;

import java.util.ArrayList;

public class Student {

    private String name;
    private ArrayList<Integer> grades = new ArrayList<Integer>();

    public Student() {

    }

    public void addGrade(int grade) {
        grades.add(grade);
    }

    public double getGradeAverage() {

        double gradesSum = 0;
        for (int grade : grades) {
          gradesSum += grade;
        }
        double gradeAvg = gradesSum/grades.size();
        return gradeAvg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Integer> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Integer> grades) {
        this.grades = grades;
    }

} // end of class
