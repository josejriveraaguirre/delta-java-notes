public class Variables {

    public static void main(String[] args) {

//        int favoriteNum = 5;
//        System.out.println(favoriteNum);
//
//        String myName = "Jose";
//        System.out.println(myName);

//        char myName = "Jose"; //error
//        System.out.println(myName);

//        String myName = 3.14159;
//        System.out.println(myName);  // error type incompatible

//        long myNum;
//        System.out.println(myNum); //error: not initialized

//        long myNum = 3.14;
//        System.out.println(myNum); // error incompatible types

//        long myNum = 123L;
//        System.out.println(myNum); //works: 123

//        long myNum = 123;
//        System.out.println(myNum); // works: 123

//        Answer: Long is for whole numbers only.

//        float myNum = 3.14F;
//        System.out.println(myNum);

//        Answer: The value 3.14 is considered a double by default. Make myNum a "double" or append the "F" to the assigned value to make it a float.

//        int x = 10;
//        System.out.println(x++); // 10
//        System.out.println(x); // 11

//        int x = 10;
//        System.out.println(++x); // 11
//        System.out.println(x); // 11

//        String class = "Test"; // error: not a statement

//        String theNumberEight = "eight";
//        Object o = theNumberEight;
//        int eight = (int) o; // class java.lang.String cannot be cast to class java.lang.Integer (java.lang.String and java.lang.Integer are in module java.base of loader 'bootstrap')

//        int eight = (int) "eight"; //  incompatible types: java.lang.String cannot be converted to int

//        int x = 5;
//        x = x + 6;
//
//        System.out.println(x += 6);
//
//        int x = 7;
//        int y = 8;
//        y = y * x;
//
//        System.out.println(y *= x);
//
        int x = 20;
        int y = 2;
//        x = x / y;
//        System.out.println(x);
//
//        x /= y;
//        System.out.println(x);
//
//        y = y - x;
//
//        y -= x;

//        System.out.println(Integer.MAX_VALUE);

//        int x = 2147483648; //error: integer number too large



    }

}
