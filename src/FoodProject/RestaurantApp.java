package FoodProject;

import java.util.Scanner;

public class RestaurantApp {
    public static void main(String[] args) {
        System.out.println("========== FOOD ==========");
        do {
            System.out.println("What do you feel like eating?\n" +
                    "0 - exit\n" +
                    "1 - view all\n" +
                    "2 - burgers\n" +
                    "3 - chicken");

            Scanner scanner = new Scanner(System.in);
            int userInput = scanner.nextInt();

            if (userInput == 1) {
                System.out.println(Restaurant.allRestaurants());
            }
            else if (userInput == 2) {
                System.out.println(Restaurant.burgerRestaurants());
            }
            else if (userInput == 0) {
                break;
            }
        } while (true);

    }
}

