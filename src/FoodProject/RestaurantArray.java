package FoodProject;

public class RestaurantArray {

    public static Restaurant[] showAllRestaurants() {
        return new Restaurant[] {
                new Restaurant("Whataburger", "burgers"),
                new Restaurant("Wendy's", "burgers"),
                new Restaurant("Pizza Hut", "pizza"),
                new Restaurant("Domino's", "pizza"),
                new Restaurant("Chick-Fil-A", "chicken"),
                new Restaurant("Popeyes", "chicken"),
                new Restaurant("Cane's", "chicken"),
                new Restaurant("Taco Cabana", "tacos"),
                new Restaurant("Chipotle", "mexican")
        };
    }
}

