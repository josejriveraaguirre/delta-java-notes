public class HelloWorld {

//    main method shorthand: "psvm" + Enter

    public static void main(String[] args) {

//        sout

        System.out.println("Hello World");
        System.out.println("Welcome to Java");

//        single line comments
        /*
        multi
        line
        comments
         */

//        STRINGS
//        need to be in "" double quotations
//        char - character
//        needs to be in '' single quotations

//        ESCAPE CHARACTERS

        System.out.println("Escape characters: ");
        System.out.println("First \\"); // backslash
        System.out.println("Second \n"); // new line
        System.out.println("Third \t"); // tab


//        VARIABLES
        /*
        All variables in JAVA must be declared before they are used.
        Syntax:
        dataType nameOfVariable;
         */

//        EXAMPLES
        byte age = 13;
        short myShort = -32768;

        int myInteger;
        myInteger = 18;

        boolean isAdmin;
        isAdmin = false;
        System.out.println(isAdmin);

        /*
        CASTING
        Turning a value of one type into another.
        Two types of casting: implicit /explicit casting.
         */

//        IMPLICIT EXAMPLE
        int myInt = 900;
        long morePrecise = myInt;
        System.out.println(morePrecise);

//        EXPLICIT EXAMPLE
        double pi = 3.14159;
        int almostPi = (int) pi;
        System.out.println(almostPi);

        int a = 2;
        int b = 5;
        System.out.println(a + b);



    }



}
