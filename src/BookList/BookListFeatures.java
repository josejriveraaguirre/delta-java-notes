package BookList;

import java.util.ArrayList;
import java.util.Scanner;

public class BookListFeatures {
    ArrayList<Book> bookList = new ArrayList<>();

    // method to add a new Book to the bookList
    public void addingBook() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a title: ");
        String titleInput = scanner.nextLine();

        System.out.println("Enter the author: ");
        String authorInput = scanner.nextLine();

        Book newBook = new Book(authorInput, titleInput);

        // how can we add 'newBook' to ArrayList 'bookList'?
        bookList.add(newBook);

        System.out.println("Book has been added successfully!");
//        System.out.println(bookList.get(newBook));

        displayList();
    }

    // method to display the book list
    public void displayList() {
        for (Book book : bookList) {
            System.out.println(book.getAuthor() + " " + book.getTitle());
        }
    }


}
