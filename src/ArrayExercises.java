import java.util.Arrays;

public class ArrayExercises {

    public static void main(String[] args) {

//  Part 1:

        int[] numbers = {3, 1, 5, 2, 4};

//        System.out.println(numbers); // returns: "[I@e580929"

        System.out.println(Arrays.toString(numbers));

//  Part 2:

        int[] numbers2 = Arrays.copyOf(numbers,numbers.length);

        Arrays.sort(numbers2);

        System.out.println(Arrays.toString(numbers2));

//  Part 3:

        int[] moreNumbers = addNumber(numbers,6);

        System.out.println(Arrays.toString(moreNumbers));

        Arrays.sort(moreNumbers);

        System.out.println(Arrays.toString(moreNumbers));

//  Part 4:

        String[] deltaCohort = new String[3];

        deltaCohort[0]= "Angela";
        deltaCohort[1]= "Alyssa";
        deltaCohort[2]= "Victor";

        for (String student : deltaCohort) {

            System.out.println(student);

        }

        System.out.println(Arrays.toString(deltaCohort));

//  Part 5:

        String[] strCopy = addToArray(deltaCohort,"Jose");

        System.out.println(Arrays.toString(strCopy));

//  Part 6:

        int[] digits = {1,2,3};

        System.out.println(sumOfArray(digits));

//  Part 7:

       String[] greetings = {"hello", "goodbye", "goodnight"};

        System.out.println(stringTogether(greetings));

    } //end of psvm

    public static int[] addNumber(int[] numArray, int num) {

        int[] numCopy = Arrays.copyOf(numArray,numArray.length + 1);

        numCopy[numCopy.length - 1] = num;

        return numCopy;
    }

    public static String[] addToArray(String[] strArray, String str) {

        String[] strCopy = Arrays.copyOf(strArray,strArray.length + 1);

        strCopy[strCopy.length - 1] = str;

        return strCopy;
    }

    public static int sumOfArray(int[] digitsArray) {

        int digitsSum = 0;

        for (int eachDigit : digitsArray) {

            digitsSum += eachDigit;


        }

        return digitsSum;

        }

    public static String stringTogether(String[] strArray) {

        String allStr = "";

        for (String eachStr : strArray) {

            allStr += eachStr;

        }

        return allStr;

    }

} // end of class
