package PolymorphismLesson;

public class Developer {

    public final String works() {
        return "I'm coding from the office";
    }

    // this prevents the ScrumMaster class from
    // inheriting the works() method
}

