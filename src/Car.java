public class Car {

    private String name;
    public String maker;
    public String model;
    public int year;

    public static void main(String[] args) {
//
////        Car car1 = new Car("Corolla", "Toyota", "CE", 2021);
//
        Car car1 = new Car();

        car1.name = "Corolla";

        car1.maker = "Toyota";

        car1.model = "CE";

        car1.year = 2020;
//
//
////        Car car2 = new Car("Camry", "Toyota", "LE", 2021);
//
        Car car2 = new Car();

        car2.name = "Camry";

        car2.maker = "Toyota";

        car2.model = "LE";

        car2.year = 2021;
//
////        Car car3 = new Car("Avalon", "Toyota", "XLE", 2021);
//
        Car car3 = new Car();

        car3.name = "Avalon";

        car3.maker = "Toyota";

        car3.model = "XLE";

        car3.year = 2021;

        System.out.println(car1.name);
        System.out.println(car1.maker);
        System.out.println(car1.model);
        System.out.println(car1.year);
        System.out.println();
        System.out.println(car2.name);
        System.out.println(car2.maker);
        System.out.println(car2.model);
        System.out.println(car2.year);
        System.out.println();
        System.out.println(car3.name);
        System.out.println(car3.maker);
        System.out.println(car3.model);
        System.out.println(car3.year);

//
//        System.out.println(car1.getCarInfo());
//        System.out.println(car2.getCarInfo());
//        System.out.println(car3.getCarInfo());
//
//
//        public String getCarInfo() {
//
//            return String.format("Car Name: %s%nMaker: %s%nModel: %s%nYear: %d%n", this.shareName(), this.maker, this.model, this.year);
//
//        }
//
//        public String shareName() {
//
//            return this.name;
//
//        }
//
//    public Car(String name, String maker, String model, int year){
//
//            this.name = name;
//
//            this.maker = maker;
//
//            this.model = model;
//
//            this.year = year;
//
//        }
//
    }
//
}
