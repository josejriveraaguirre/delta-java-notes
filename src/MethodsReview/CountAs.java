package MethodsReview;

import java.util.Scanner;

public class CountAs {

//    Write a Java method to count all the a's in a string.

    public static int countingTheA(String str) {
        int counter = 0;

        for (int i = 0; i < str.length(); i++ ) {
            if (str.charAt(i) == 'a') {
                counter++;
            }
        }
        return counter;
    }

    public static void main(String[] args) {
//        System.out.println(countingTheA("bravo"));

        // use the Scanner class
        Scanner scanner = new Scanner(System.in);
        System.out.println("================");
        System.out.println("Enter a string: ");

        String userInput = scanner.nextLine().toLowerCase();

        System.out.println("You entered: " + userInput);
        System.out.println("Number of A's = " + countingTheA(userInput));
        System.out.println("================");

    }
}
