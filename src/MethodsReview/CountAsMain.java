package MethodsReview;

import java.util.Scanner;

public class CountAsMain {
    public static void main(String[] args) {
        // instantiate the object of the class name
        CountAs count = new CountAs();


        Scanner scanner = new Scanner(System.in);
        System.out.println("================");
        System.out.println("Enter a string: ");

        String userInput = scanner.nextLine().toLowerCase();

        System.out.println("You entered: " + userInput);
        System.out.println("Number of A's = " + count.countingTheA(userInput));
        System.out.println("================");

//        CountAs count2 = new CountAs();
//        System.out.println(count2.countingTheA("whateverf"));

//        CountAs bob = new CountAs();
//        System.out.println(bob.countingTheA("hi bob"));
    }
}
