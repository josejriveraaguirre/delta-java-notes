package MethodsReview;

public class MethodLectureTest {
    public static void main(String[] args) {
        // create an object our MethodLecture class
        // "instantiating an object"
        /*
        ClassName objectName = new ClassName(arguments);
         */
        MethodLecture m1 = new MethodLecture();
        System.out.println(m1.printNumber()); // returns 1

        m1.sayHello(); // return "Hello"

        System.out.println(m1.printFive()); // returns 5


        // access yell(String str) from the MethodLecture class and print a string value?
        System.out.println(m1.yell("message here"));


        // access the yell() method from the MethodLecture class
        // that print the return statement of the method that does not have any parameter
        System.out.println("MethodLectureTest: " + m1.yell());
    }
}
