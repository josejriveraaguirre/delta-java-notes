package ExceptionsAndErrorHandling;

public class ExceptionsLesson {

    /*
    EXCEPTIONS / ERROR HANDLING
    -allows us to control the flow of a program when the errors occur
    -there are different types of exceptions, when and how to use them, and how to create custom exceptions.

    An exception is an unexpected event that occurs during a program's execution.
    -it affects the program instructions which can cause the program to terminate.

    An exceptioin can occur for many reasons:
    Invalid user input
    Device failure
    Loss of network connection
    Physical limitations (disk memory capacity)
    Code errors
    Opening an unavailable file

    Try-catch-finally block

    SYNTAX:

    try {
    do something
    }
    catch (type of error) {
    do something else when an error occurs
    }
    finally {
    do something regardless of whether the try or catch condition is met
    }
     */

    public static void main(String[] args) {
        //example of try catch block
//        try {
//            int divideByZero = 6/0;
//            System.out.println("Code in try block executed successfully!");
//        }
//        catch(Exception e) {
//            System.out.println("Exception: " + e.getMessage());
//            // display the default built-in message for Exception
//        }

        class ListOfNumbers {

            public int[] arrayOfNumbers = new int[10];

            public void writeList() {
                try{
                    arrayOfNumbers[9] = Integer.parseInt("eleven");
                    arrayOfNumbers[10] = Integer.parseInt("11");
                    System.out.println("Try code block executed");
                }
                catch (NumberFormatException e1) {
                    System.out.println("NumberFormatException: " + e1.getMessage());
                }
                catch (IndexOutOfBoundsException e2) {
                    System.out.println("IndexOutOfBoundsException: " + e2.getMessage());
                }
                finally {
                    System.out.println("Finally block executed, no matter what!");
                }

            }
        } // end of ListOfNumbers class

//        try {
            ListOfNumbers list = new ListOfNumbers();
            list.writeList();
//        }
//        catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//        finally {
//            System.out.println("Finally block executed, no matter what!");
//        }

    } // end of psvm
} // end of class
