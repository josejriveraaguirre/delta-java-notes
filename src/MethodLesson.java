public class MethodLesson {

    public static void main(String[] args) {

        int sum = addNumbers(2,3);

        System.out.println("Using our static method:");

        System.out.println(sum);


        System.out.println(tenureMessage("Stephen", "Java", 3));

        System.out.println(tenureMessage("Angular"));

        sayHello();

        System.out.println();

        sayHello(5);

        System.out.println();

        sayHello("Delta");

        System.out.println();

        sayHello("Howdy", "Zion");


    } // end of psvm

//    Method Structure

    public static int addNumbers(int num1, int num2) {

        return num1 + num2;

    }

    public static int substractNumbers(int num1, int num2) {

        return num1 - num2;

    }

    public void greetings() {

//        can be static or non-static. They don't return anything.

        System.out.println("Hello, good day!");

    }

//    start of tenureMessage method

    public static String tenureMessage(String name, String progLang, int numYears) {

        return name + " has been coding " + progLang + " for " + numYears + " years.";

    }

//    **************** METHOD OVERLOADING ***************************

    public static String tenureMessage(String progLang) {

        return "Somebody is good at " + progLang + ".";

    }

//    sayHello version 1:

    public static void sayHello(int times) {

        for (int i = 0; i < times; i++) {

            sayHello();
        }

    }

    //    version 2:

    public static void sayHello() {

        sayHello("Hello", "World");

    }

//    versdion 3:

    private static void sayHello(String greeting, String name) {

        System.out.printf("%s %s!",greeting, name);
    }

//    version 4:

    private static void sayHello(String name) {

       sayHello("Hello", name);
    }


} // end of class
