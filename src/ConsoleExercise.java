import java.util.Scanner;

public class ConsoleExercise {

    public static void main(String[] args) {

//        double pi = 3.14159;
//
//        System.out.printf("The value of pi is approximately %.2f", pi);
//
//        System.out.println();

          Scanner scanner = new Scanner(System.in);

//        System.out.println("Please enter an integer: ");
//
//        Integer userInt = scanner.nextInt();
//
//        System.out.println("The integer you entered was: " + userInt);

//        answer: InputMismatchException error

//        System.out.println("Please enter three words: ");
//
//        String userString1 = scanner.nextLine();
//        String userString2 = scanner.nextLine();
//        String userString3 = scanner.nextLine();
//
//        System.out.println("Thank you, you entered: \n" + userString1 + "\n" + userString2 + "\n" + userString3 + "\n");

//        answer: less: it does not run the next line of code. more: you don't get to because it runs the next line of code.

//        System.out.println("Please enter a sentence: ");
//
//        String userSentence = scanner.next();
//
//        System.out.println("Thank you, you entered: \n" + userSentence);

//        answer: only the first word

//        System.out.println("Please enter a sentence: ");
//
//        String userSentence = scanner.nextLine();
//
//        System.out.println("Thank you, you entered: \n" + userSentence);

        System.out.println("Please enter the length of the CodeBound classroom (in ft): ");

        double userLength = scanner.nextDouble();

        System.out.println("Please enter the width of the CodeBound classroom (in ft): ");

        double userWidth = scanner.nextDouble();

        double classroomArea = userLength * userWidth;

        System.out.println();

        System.out.println("Classroom Area is: " + classroomArea + " sq ft");

        double classroomPerimeter = 2 * (userLength + userWidth);

        System.out.println();

        System.out.println("Classroom Perimeter is: " + classroomPerimeter + " ft");

        System.out.println();

        System.out.println("Now, please enter the height of the CodeBound classroom (in ft): ");

        double userHeight = scanner.nextDouble();

        double classroomVolume = classroomArea * userHeight;

        System.out.println();

        System.out.println("Classroom Volume is: " + classroomVolume + " cu ft");


    } // end of method

} // end of class
