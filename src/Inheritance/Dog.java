package Inheritance;
/*
How do we INHERIT from another class?
- use the 'extends' keyword
 */
public class Dog extends Animal {
    // FIELDS for our Dog class
    private int eyes;
    private int legs;
    private int tail;
    private int teeth;
    private String fur;


//    public Dog(String name, int brain, int body, int size, int weight) {
//        super(name, brain, body, size, weight); // this line must be the first in the CHILD's constructor
//        // ^ this is initializing the BASE/PARENT/ SUPER characteristics of an Animal
//    }

    public Dog(String name, int brain, int body, int size, int weight, int eyes, int legs, int tail, int teeth, String fur) {
        super(name, brain, body, size, weight);
        this.eyes = eyes;
        this.legs = legs;
        this.tail = tail;
        this.teeth = teeth;
        this.fur = fur;
    }

    // method
    private void chew() {
        System.out.println("This dog is chewing their food");
    }

    // OVERRIDING METHODS - inherits a method(s) but makes it unique for this class
    @Override
    public void eat() {
        System.out.println("This dog is eating...");
//        chew();
        this.chew();

        // call the Animal's eat() inside of the Dog's eat()?
        super.eat();
    }

    /*
    super keyword- allows us to access a superclass' methods and constructors within a
    subclass.
     */




}
