package Inheritance;

public class AnimalMain {
    public static void main(String[] args) {
        // instantiate a animal object = "create a new object of a class"
        // ClassName objectName = new ClassName(arguments);

        // create a new Animal object
        Animal animal1 = new Animal("Animal", 1, 1, 5, 10);

        // create a new Dog object
        Dog dog1 = new Dog("Spot", 1, 1, 5, 25, 2, 4, 1, 25,"long silky");

        // call a method from the dog class
        dog1.eat();

        // call a method from the animal class
        animal1.eat();
    }
}

