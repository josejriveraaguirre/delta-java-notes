//package shapes;
//
//import validation.Input;
//
//import java.util.Scanner;
//import static validation.Input.getDouble;
//import static validation.Input.yesNo;
//
//public class CircleApp {
//
//    public static void main(String[] args) {
//
//        String userInput = "";
//        boolean answer;
//
//        do {
//
//            System.out.print("\nPlease enter the radius of the circle: ");
//
//            double circleRadius = Input.getDouble();
//
//            Circle c1 = new Circle(circleRadius);
//
//            System.out.printf("%nThe circumference of the circle is: %.2f%n", c1.getCircumference());
//
//            System.out.printf("%nThe area of the circle is: %.2f%n", c1.getArea());
//
////            System.out.print("\nWould you like to make another circle? [y/n]: ");
//
//            answer = Input.yesNo(userInput);
//
////            System.out.println(answer);
//
//        } while (answer);
//
//    }
//
//}

// AFTER WALKTHROUGH

//package shapes;
//
//        import org.w3c.dom.ls.LSOutput;
//        import validation.Input;
//
//        import java.util.Scanner;
//
//public class CircleApp {
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        Input input = new Input(scanner);
//        System.out.println("Please enter a radius: ");
//
//
//        double radius = input.getDouble();
//        Circle circle = new Circle(radius);
//
//        System.out.println("circle.getArea() = " + circle.getArea());
//        System.out.println("circle.getCircumference = " + circle.getCircumference());
//
//    }
//}
