package shapes;

public class ShapesMain {

    public static void main(String[] args) {

//        Rectangle box1 = new Rectangle(5,4);
//
//        Rectangle box2 = new Square(5);
//
//        box1.getPerimeter();
//
//        box1.getArea();
//
//        box2.getPerimeter();
//
//        box2.getArea();

        Measurable myShape = new Measurable() {
            @Override
            public double getPerimeter() {
                return 0;
            }

            @Override
            public double getArea() {
                return 0;
            }
        };

        Measurable myShape1 = new Square(6);

        Measurable myShape2 = new Rectangle(7,6);

        System.out.println("\nMyShape1's area is: " + myShape1.getArea());
        System.out.println("\nMyShape1's perimeter is: " + myShape1.getPerimeter());
        System.out.println("\n\nMyShape2's area is: " + myShape2.getArea());
        System.out.println("\nMyShape2's perimeter is: " + myShape2.getPerimeter() + "\n");

    } // end of psvm

} // end of class
