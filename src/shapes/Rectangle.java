package shapes;

//public class Rectangle {
//
////    FIELDS
//
//    protected double length;
//
//    protected double width;
//
////    CONSTRUCTOR
//
//    public Rectangle(double length, double width) {
//
//        this.length = length;
//
//        this.width = width;
//
//    }
//
//    public void getArea() {
//
//        System.out.println(this.length * this.width);
//
//    }
//
//    public void getPerimeter() {
//
//        System.out.println(2 * (this.length + this.width));
//
//    }
//
//}

public class Rectangle extends Quadrilateral implements Measurable {

    public Rectangle(double length, double width) {
        super(length, width);
    }

    @Override
    public double getPerimeter() {
        return 2 * (length + width);
    }

    @Override
    public double getArea() {
        return length * width;
    }

    @Override
    public void setLength() {

    }

    @Override
    public void setWidth() {

    }

} // end of class
