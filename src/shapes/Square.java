package shapes;

//public class Square extends Rectangle {
//
////    protected double side;
//
//    public Square(double side) {
//
//        super(side,side);
//
////        this.side = side;
//
//    }
//
//    @Override
//    public void getArea() {
//
//        System.out.println(Math.pow(this.length, 2));
//
//
//    }
//    @Override
//    public void getPerimeter() {
//
//        System.out.println(4 * this.length);
//
//    }
//}

public class Square extends Quadrilateral {

    public Square(double side) {
        super(side, side);
    }

    @Override
    public double getPerimeter() {
        return 4 * length;
    }

    @Override
    public double getArea() {
        return Math.pow(length, 2);
    }

    @Override
    public void setLength() {

    }

    @Override
    public void setWidth() {

    }
}
