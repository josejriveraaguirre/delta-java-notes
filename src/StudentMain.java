public class StudentMain {

    public static void main(String[] args) {

//        create two students

        Student s1 = new Student("Karen");

        Student s2 = new Student("Miguel", "Alpha");

        Student s3 = new Student("KC", "Bravo", 91.5);

        System.out.println(s1.name);

        System.out.println(s2.sayHello());

        System.out.println(s2.getStudentInfo());

        System.out.println(s3.getStudentInfo());

        System.out.println(s3.shareGrade());
    }
}
