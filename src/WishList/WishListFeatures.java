package WishList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class WishListFeatures {

    ArrayList<Wish> wishList = new ArrayList<>();
    ArrayList<String> wishListAlpha = new ArrayList<>();

    public void addingWish() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the item's name: ");
        String nameInput = scanner.nextLine();

        System.out.println("Enter the item's price: ");
        double priceInput = scanner.nextDouble();

        Wish newWish = new Wish(nameInput, priceInput);

        wishList.add(newWish);

        System.out.println("\nThe item has been added successfully!\n");

        displayList();

    }


    public void displayList() {

        if (wishList.size() == 0) {

            System.out.println("0 items\n");

        }
        else {

            System.out.println(wishList.size() + " item(s)\n");

            for (Wish wish : wishList) {
                System.out.println(wish.getName() + " - " + wish.getPrice());
            }

            System.out.println();

        }

    }

    public void displayListAlpha() {

        if (wishList.size() == 0) {

            System.out.println("0 items\n");

        }
        else {

            System.out.println(wishList.size() + " item(s)\n");

            for (Wish wish : wishList) {
                wishListAlpha.add(wish.getName() + " - " + wish.getPrice());
            }

            Collections.sort(wishListAlpha);

            for (String wishAlpha : wishListAlpha) {
                System.out.println(wishAlpha);
            }

            System.out.println();

        }

    }

    public void displayMenu() {

        System.out.printf("%n========== WISHLIST APP ==========%n%n");

        do {

            System.out.printf("What would you like to do?%n" +

                    "%n1 - Add an Item%n" +
                    "2 - Display All%n" +
                    "3 - Display All in Alphabetical Order%n" +
                    "0 - Exit%n");

            Scanner scanner = new Scanner(System.in);

            System.out.printf("%nPlease enter your selection: ");

            int userChoice = scanner.nextInt();

            System.out.println();

            scanner.nextLine(); // To "kill" the java scanner "bug"

            if (userChoice == 1) {

                addingWish();

            }
            else if (userChoice == 2) {

                displayList();

            }
            else if (userChoice == 3) {

                displayListAlpha();

            }
            else if (userChoice == 0) {

                    System.out.printf("Goodbye!%n");

                    break;
                }
        } while (true);
    }
} // end of class
