import java.util.Scanner;

public class ConsoleIO {

    public static void main(String[] args) {


//        String cohort = "Delta";
//        System.out.println(cohort);
//
//        String greeting = "Bonjour";
//        System.out.println(greeting);
//
////        println vs. print
//
//        System.out.print(cohort);
//        System.out.print(greeting);
//        System.out.println();
//
////        souf printf/ format
//
//        System.out.printf("%s, %s!%n", greeting, cohort);
//        System.out.format("%s, %s!", greeting, cohort);

//double price1 = 23.451;
//double price2 = 50;
//double price3 = 5.40;
//double total = (price1 + price2 + price3);
//
//        System.out.println(total);
//        System.out.printf("Your total: $%7.2f%n", total);
//        System.out.printf("Your total: $%7.3f%n", total);
//        System.out.printf("Your total: $%7f%n", total);
//        System.out.printf("Your total: $%.2f%n", total);

//        SCANNER CLASS - get input from console.

        Scanner scanner = new Scanner(System.in);

//        create a prompt

//        System.out.println("Enter your name: ");
//
//        String userInput = scanner.nextLine();
//
//        System.out.println("Thank you, you entered: \n" + userInput);
//
        System.out.println("Enter an integer: ");

//        Integer userInt = scanner.nextInt();

//        or

//        int userInt = scanner.nextInt();

//        or

        double userInt = scanner.nextDouble();

//        System.out.println("The integer you entered was: " + userInt);

//        or

        System.out.printf("The integer you entered was: %.2f", userInt);







    }

}
