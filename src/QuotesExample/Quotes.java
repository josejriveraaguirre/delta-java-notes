package QuotesExample;


import java.util.Random;
import java.util.Scanner;

public class Quotes {
    public static String[] QUOTES = {
            "Be yourself; everyone else is already taken.― Oscar Wilde",
            "A room without books is like a body without a soul. ― Marcus Tullius Cicero",
            "Be the change that you wish to see in the world. ― Mahatma Gandhi",
            "If you tell the truth, you don't have to remember anything. ― Mark Twain",
            "If you want to know what a man's like, take a good look at how he treats his inferiors, not his equals.― J.K. Rowling",
            "To live is the rarest thing in the world. Most people exist, that is all.― Oscar Wilde",
            "Without music, life would be a mistake. ― Friedrich Nietzsche",
            "Always forgive your enemies, nothing annoys them so much. ― Oscar Wilde",
            "Life isn't about getting and having, it's about giving and being. –Kevin Kruse",
            "Whatever the mind of man can conceive and believe, it can achieve. –Napoleon Hill",
            "Strive not to be a success, but rather to be of value. –Albert Einstein",
    };

    // create a method that will print out a random quote from our QUOTES array
    public static void printQuote() {
//        // import the Random class
//        Random random = new Random();
//
//        // option 1
////        System.out.println(QUOTES[random.nextInt(QUOTES.length)]);
//        //                  QUOTES[generate a random index];
//
//        // option 2
//        int randomNumber = (int) Math.floor(Math.random() * QUOTES.length);
//        System.out.println(QUOTES[randomNumber]);

        // updated version
        Scanner scanner = new Scanner(System.in);
        System.out.println("Quote of the day");
        do {
            Random random = new Random();
            System.out.println(QUOTES[random.nextInt(QUOTES.length)]);

        } while (yesNo(scanner).equalsIgnoreCase("y"));
        System.out.println("Live freely. Bye.");
    }

    public static String yesNo(Scanner scanner) {
        String userChoice;
        do {
            System.out.println("Would you like another quote? [y/n]: ");
            userChoice = scanner.next();
        } while (!userChoice.equalsIgnoreCase("y") && !userChoice.equalsIgnoreCase("n"));
        return userChoice;
    }


    public static void main(String[] args) {
        printQuote();
    }
}
