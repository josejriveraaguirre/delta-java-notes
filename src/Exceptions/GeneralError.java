package Exceptions;

import java.util.Scanner;

public class GeneralError {
    public static void main(String[] args) {

        int numerator;
        int denominator;

        Scanner userInput = new Scanner(System.in);

        try {

            System.out.println("Enter the numerator:");
            numerator = userInput.nextInt();
            System.out.println("Enter the denominator");
            denominator = userInput.nextInt();
            System.out.println(numerator/denominator);

        }
        catch (Exception e) {

            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("---End of Error Handling Example---");
        }

        // Answer: 3
        // Answer: / by zero
        // Answer: because division by zero being undefined is a the general exception that was caught.

    } // end of psvm
} // end of class
