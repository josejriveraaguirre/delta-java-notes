package Exceptions;

import java.util.Scanner;

public class Bonus {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a binary number: ");

        String userInput1 = scanner.nextLine();

        getBinary(userInput1);

        System.out.print("Enter a hexadecimal number: ");

        String userInput2 = scanner.nextLine();

        getHexadecimal(userInput2);


    } // end of psvm

    public static void getBinary(String userI1) {

        String bin = userI1;
        int num1 = Integer.valueOf(bin,2);
        System.out.println("Your number is: " + num1);
    }
    public static void getHexadecimal(String userI2) {

        String hex = userI2;
        int num2 = Integer.valueOf(hex,16);
        System.out.println("Your number is: " + num2);
    }

} // end of class
