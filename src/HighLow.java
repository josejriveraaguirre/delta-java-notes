import java.util.Scanner;

public class HighLow {

    public static void main(String[] args) {

        highLow();

    } // end of psvm

    public static void highLow() {

        int rand = (int) Math.floor(Math.random()*(100-1+1)+1);

        System.out.printf("%n%nC:\\Users\\jaguirre\\.jdks\\corretto-11.0.10\\bin\\java.exe -javaagent:C:\\Program Files\\JetBrains\\IntelliJ IDEA 2020.3.2\\lib\\idea_rt.jar=53%d887:C:\\Program Files\\JetBrains\\IntelliJ IDEA 2020.3.2\\bin -Dfile.encoding=UTF-8 -classpath C:\\Users\\jaguirre\\IdeaProjects\\delta-java-notes\\out\\production\\delta-java-notes HighLow%n", rand);

        System.out.println("\nI'm thinking of an integer number from 1 to 100. I'll give you five guesses:\n\n\n\n\n\n");

        Scanner scanner = new Scanner(System.in);

        int i = 1;

        boolean compare;

        do {

            int userNum = scanner.nextInt();

            compare = userNum == rand;

            if (userNum < rand && i != 5) {

                System.out.printf("\nGo higher! Guesses left: %d%n", (5-i));

            } else if (userNum > rand && i != 5){

                System.out.printf("\nGo lower! Guesses left: %d%n", (5-i));

            }

            i++;

        } while (i <= 5 == true && compare == false);

        if (compare == true) {

            System.out.println("\nYOU GUESSED IT!!! Wait... are you PSYCHIC?!");

        } else System.out.println("\nSorry!");

    }

} // end of class
