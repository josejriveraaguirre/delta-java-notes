package ObjectsReview;

import java.util.Scanner;

import static ObjectsReview.Television.*;

public class TelevisionMain {

    public static void main(String[] args) {


        Television show1 = new Television("Hell's Kitchen", 2013,"FilmRise");

        Television show2 = new Television("Kitchen Nightmares", 2014,"Channel4");

        Television show3 = new Television("Master Chef", 2015,"Paramount");

        System.out.printf("%n%s came out in %d, and was produced by %s.%n", show1.getTitle(),show1.getYear(), show1.getStudio());

        System.out.printf("%n%s came out in %d, and was produced by %s.%n", show2.getTitle(),show2.getYear(), show2.getStudio());

        System.out.printf("%n%s came out in %d, and was produced by %s.%n", show3.getTitle(),show3.getYear(), show3.getStudio());

//        System.out.println(show1.displayInfo());
//
//        System.out.println(show2.displayInfo());
//
//        System.out.println(show3.displayInfo());

        Scanner scanner = new Scanner(System.in);

        System.out.println("\nPlease enter the title:");

        String userInput1 = scanner.nextLine();

        System.out.println("\nPlease enter the year:");

        int userInput2 = scanner.nextInt();

        scanner.nextLine(); // ***NECESSARY TO AVOID SKIPPING OF THE FOLLOWING LINE, DUE TO JAVA LEAVING THE CURSOR
        // IN THE SAME LINE AS THE INTEGER, SO IT NEEDS TO BE SPENT BEFORE SCANNING THE NEXT LINE.***

        System.out.println("\nPlease enter the studio:");

        String userInput3 = scanner.nextLine();

        Television show4 = new Television(userInput1, userInput2, userInput3);

        System.out.printf("%n%s came out in %d, and was produced by %s.%n", show4.getTitle(),show4.getYear(), show4.getStudio());

    } // end of psvm

} // end of class
