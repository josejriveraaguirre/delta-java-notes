package ObjectsReview;

public class StaffMain {
    public static void main(String[] args) {

        // ClassName objectName = new ClassName(arguments);
//        Staff staff1 = new Staff();


        Staff staff2 = new Staff("Jane");
        staff2.setHoursWorked(160);
        int pay = staff2.calculatePay(1000, 400);
        System.out.println("Hours Worked: " + staff2.getHoursWorked());
        System.out.println("Pay : $" + pay);
//        staff2.hoursWorked = 160; ERROR!


        Staff staff3 = new Staff("Kevin", "Smith");
        staff3.setHoursWorked(160);
        int payStaff3 = staff3.calculatePay();
        System.out.println("Hours Worked: " + staff3.getHoursWorked());
        System.out.println("Staff3 pay: $" + payStaff3);

        Staff staff4 = new Staff("Travis");
        staff4.setHoursWorked(-10);
//        int payStaff4 = staff4.calculatePay();
        System.out.println("Pay " + staff4.calculatePay());


    }
}

