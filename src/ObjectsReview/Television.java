package ObjectsReview;

import java.util.Scanner;

public class Television {

    private String title;

    private int year;

    private String studio;

//    private Scanner scanner = new Scanner(System.in);
//
//    String userInput1 = scanner.nextLine();
//
//    int userInput2 = scanner.nextInt();
//
//    String userInput3 = scanner.nextLine();

//    public Television() {
//
//        System.out.println("\nTelevision object being created...");
//    }

//    public Television(String showTitle) {
//
//        System.out.println("\nTelevision object created:");
//
//        this.title = showTitle;
//
//        System.out.printf("%n%s%n",title);
//
//    }

//    public Television(String showTitle, int firstYear) {
//
//        System.out.println("\nTelevision object created:");
//
//        this.title = showTitle;
//
//        this.year = firstYear;
//
//        System.out.printf("%nTitle: %s",title);
//
//        System.out.printf("%nYear: %s%n",year);
//
//    }

//    CONSTRUCTOR:

    public Television(String showTitle, int firstYear, String prodComp) {

//        System.out.println("\nTelevision object created:");

        this.title = showTitle;

        this.year = firstYear;

        this.studio = prodComp;

//        System.out.printf("%nTitle: %s",title);
//
//        System.out.printf("%nYear: %s",year);
//
//        System.out.printf("%nStudio: %s%n",studio);

    }

    public String displayInfo() {

        return String.format("%n%s came out in %d, and was produced by %s.%n", title, year,studio);

    }

    public String getTitle() {

//        this.title = userInput1;

        return title;
    }

    public int getYear() {

//        this.year = userInput2;

        return year;
    }

    public String getStudio() {

//        this.studio = userInput3;

        return studio;
    }

//    public void setTitle(String showTitle) {
//        title = showTitle;
//    }
//
//    public void setYear(int firstYear) {
//        year = firstYear;
//    }
//
//    public void setStudio(String prodComp) {
//        studio = prodComp ;
//    }

    public void setTitle(String userInput1) {

        this.title = userInput1;
    }

    public void setYear(int userInput2) {

        this.year = userInput2;
    }

    public void setStudio(String userInput3) {

        this.studio = userInput3 ;
    }

} // end of class
