package ObjectsReview;

public class Math {
    public int a;
    public int b;

    // static variable
    public static double pi = 3.14159;

    // static methods
    public static int add(int x, int y) {
        return x + y;
    }

    public static int multiply(int x, int y) {
        return x * y;
    }
}
