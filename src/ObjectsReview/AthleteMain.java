package ObjectsReview;

public class AthleteMain {
    public static void main(String[] args) {
        // instantiate the object of our class
        // ClassName objectName = new ClassName();
        Athlete tim = new Athlete();
        tim.firstName = "Tim";
        tim.lastName = "Duncan";

        System.out.println(tim.sayHello());
        // expected output: Hello from Tim Duncan

        tim.jerseyNumber = 21;
        System.out.println(tim.jerseyNumber);
        tim.team = "San Antonio Spurs";

        Athlete michael = new Athlete();
        michael.firstName = "Michael";
        michael.lastName = "Jordan";
        System.out.println(michael.sayHello());
    }
}
