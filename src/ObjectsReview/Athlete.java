package ObjectsReview;

public class Athlete {
    // fields
    // - are variables or methods that belongs to an object or a class
    // - can be accessed with the . operator
    // EX: Athlete athlete1 = new Athlete();         -> athlete1.nameOfVariable or athlete1.nameOfMethod()
    // - by default fields belongs to objects, but can be defined as belonging to a class with the "static" keyword.
    public String firstName;
    public String lastName;
    public int jerseyNumber;
    public String team;

    public String sayHello() {

        return String.format("Hello from %s %s", firstName, lastName);
    }
}

