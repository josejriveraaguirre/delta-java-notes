package ObjectsReview;

public class HolidayMain {

    public static void main(String[] args) {

        Holiday holiday = new Holiday("Halloween", 31, "October");

        System.out.printf("%n%s is a holiday on %s %d.%n", holiday.getName(), holiday.getMonth(), holiday.getDay());

    } // end of psvm

} // end of class
