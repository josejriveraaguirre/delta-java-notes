package ObjectsReview;

public class Staff {
    private String nameOfStaff;
    private final int hourlyRate = 30;
    private int hoursWorked;

    // public, private, protected = Visibility slide 15 / 16
    // the final keyword indicates that the value cannot be changed after it is created.

    // CONSTRUCTOR - special method that is called when an object is created.
    // - can accept parameters and be overloaded.
    public Staff() {
        System.out.println("Staff object being created...");
    }

    public Staff(String name) {
        this.nameOfStaff = name;
        System.out.println("\n" + nameOfStaff);
        System.out.println("====================");
    }

    public Staff(String firstName, String lastName) {
        this.nameOfStaff = firstName + " " + lastName;
        System.out.println("\n" + nameOfStaff);
        System.out.println("====================");
    }

    public void printMessage() {
        System.out.println("Calculating Pay...");
    }

    public int calculatePay() {
        printMessage(); // return "Calculating Pay..."

        int staffPay;
        staffPay = hoursWorked * hourlyRate;

        if (hoursWorked > 0) {
            return staffPay;
        }
        else {
            return 0;
        }
    }

    // method overloading - same method name, different set of parameters
    public int calculatePay(int bonus, int allowance) {
        printMessage();

        if (hoursWorked > 0) {
            return hoursWorked * hourlyRate + bonus + allowance;
        }
        else {
            return 0;
        }
    }

    // GETTERS/ SETTERS METHODS
    public void setHoursWorked(int hours) {
        if (hours > 0) {
            hoursWorked = hours;
        }
        else {
            System.out.println("ERROR: Hours cannot be smaller than zero");
            System.out.println("ERROR: HoursWorked is not updated");
        }
    }

    public int getHoursWorked() {
        return hoursWorked;
    }
}

