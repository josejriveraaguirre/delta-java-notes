package ObjectsReview;

public class Holiday {

private String name;

private int day;

private String month;

public Holiday(String holiName, int holiDay, String holiMonth) {

    this.name = holiName;

    this.day = holiDay;

    this.month = holiMonth;

}

public String getName() {

    return this.name;
}

public int getDay() {

    return this.day;
}

public String getMonth() {

    return this.month;
}


} // end of class
