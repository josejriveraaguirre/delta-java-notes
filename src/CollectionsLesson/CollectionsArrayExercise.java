package CollectionsLesson;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionsArrayExercise {

    public static void main(String[] args) {


        ArrayList<Integer> integers = new ArrayList<>();

        integers.add(1);
        integers.add(2);
        integers.add(3);

        System.out.println(integers);

        ArrayList<String> strings = new ArrayList<>();

        strings.add("one");
        strings.add("two");
        strings.add("three");

        System.out.println(strings);

        ArrayList<String> deltaCohort = new ArrayList<>();

        deltaCohort.add("Alyssa");
        deltaCohort.add("Angela");
        deltaCohort.add("Jose");
        deltaCohort.add("Victor");

        for (String d : deltaCohort){

        System.out.println(d);

        }

        ArrayList<String> tvNets = new ArrayList<>();

        tvNets.add("CBS");
        tvNets.add("NBC");

        System.out.println(tvNets);

        tvNets.add(0,"ABC");

        System.out.println(tvNets);

        tvNets.add("FOX");

        System.out.println(tvNets);

        tvNets.remove(2);

        System.out.println(tvNets);

        ArrayList<String> dogBreeds = new ArrayList<>();

        dogBreeds.add("Cavalier King Charles Spaniel");
        dogBreeds.add("Bernese Mountain Dog");
        dogBreeds.add("Alaskan Malamute");
        dogBreeds.add("Boston Terrier");

        System.out.println(dogBreeds);

        Collections.sort(dogBreeds);

        System.out.println(dogBreeds);

        ArrayList<String> catBreeds = new ArrayList<>();

        catBreeds.add("Abyssinian");
        catBreeds.add("Birman");
        catBreeds.add("Burmese");
        catBreeds.add("Maine Coon");

        System.out.println(catBreeds);

        System.out.println(catBreeds.indexOf("Burmese"));

        Collections.reverse(catBreeds);

        System.out.println(catBreeds);

    } // end of psvm

} // end of class
