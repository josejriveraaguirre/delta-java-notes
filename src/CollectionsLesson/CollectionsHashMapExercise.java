package CollectionsLesson;

import java.util.Collections;
import java.util.HashMap;

public class CollectionsHashMapExercise {

    public static void main(String[] args) {

        HashMap<String, String> dependents = new HashMap<>();

        dependents.put("Carla", "daughter");
        dependents.put("Lucas", "son");

        System.out.println(dependents);

        dependents.put("Marco", "son");

        System.out.println(dependents);

        dependents.forEach((k, v) -> System.out.println(k + " : " + (v)));

        System.out.println(dependents.isEmpty() ? "dependents is empty" : "dependents is not empty");

//        dependents.clear();
//
//        System.out.println(dependents.isEmpty() ? "dependents is empty" : "dependents is not empty");

        System.out.println(dependents.get("Lucas"));

        System.out.println(dependents.containsKey("Carla"));

        System.out.println(dependents.containsValue("daughter"));

        System.out.println(dependents.keySet());

        System.out.println(dependents.values());






    }
}
