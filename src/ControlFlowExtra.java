import java.util.Scanner;

public class ControlFlowExtra {

    public static void main(String[] args) {

//        EXERCISE 1:

        Scanner scanner = new Scanner(System.in);

//        System.out.print("\nPlease enter a number: ");
//
//        double userNum = scanner.nextDouble();
//
//        if (userNum > 0) {
//
//            System.out.printf("\n%.2f is a positive number.", userNum);
//
//        } else if (userNum < 0) {
//
//            System.out.printf("\n%.2f is a negative number.", userNum);
//
//        } else System.out.println("\nYou entered 0, which is neither positive nor negative.");

//        EXERCISE 2:

//        System.out.println("\nPlease enter three different numbers pressing enter after each one: \n");
//
//        double userNum1 = scanner.nextDouble();
//
//        double userNum2 = scanner.nextDouble();
//
//        double userNum3 = scanner.nextDouble();
//
//        if (userNum1 > userNum2 && userNum1 > userNum3) {
//
//            System.out.printf("\nThe greatest of your three numbers is: %.2f", userNum1);
//
//        } else if (userNum2 > userNum1 && userNum2 > userNum3) {
//
//            System.out.printf("\nThe greatest of your three numbers is: %.2f", userNum2);
//
//        } else if (userNum3 > userNum1 && userNum3 > userNum2) {
//
//            System.out.printf("\nThe greatest of your three numbers is: %.2f", userNum3);
//
//        }
//
//        System.out.println();

//        EXERCISE 3:

//        System.out.print("\nPlease enter a number from 1 to 7: ");
//
//        int userNum = scanner.nextInt();
//
//        if (userNum == 1) {
//
//            System.out.println("\nSunday");
//
//        } else  if (userNum == 2) {
//
//            System.out.println("\nMonday");
//
//        } else  if (userNum == 3) {
//
//            System.out.println("\nTuesday");
//
//        } else  if (userNum == 4) {
//
//            System.out.println("\nWednesday");
//
//        } else  if (userNum == 5) {
//
//            System.out.println("\nThursday");
//
//        } else  if (userNum == 6) {
//
//            System.out.println("\nFriday");
//
//        } else  if (userNum == 7) {
//
//            System.out.println("\nSaturday");
//
//        } else  System.out.printf("\n%d is not a number from 1 to 7", userNum);

//      EXERCISE 4:

//        System.out.print("\nPlease enter a month number [1 - 12]: ");
//
//        int userNum = scanner.nextInt();
//
//        if (userNum == 1) {
//
//            System.out.println("\nJanuary has 31 days.");
//
//        } else  if (userNum == 2) {
//
//            System.out.println("\nFebruary has 28 days.");
//
//        } else  if (userNum == 3) {
//
//            System.out.println("\nMarch has 31 days.");
//
//        } else  if (userNum == 4) {
//
//            System.out.println("\nApril has 30 days.");
//
//        } else  if (userNum == 5) {
//
//            System.out.println("\nMay has 31 days.");
//
//        } else  if (userNum == 6) {
//
//            System.out.println("\nJune has 30 days.");
//
//        } else  if (userNum == 7) {
//
//            System.out.println("\nJuly has 31 days.");
//
//        } else  if (userNum == 8) {
//
//            System.out.println("\nAugust has 31 days.");
//
//        } else  if (userNum == 9) {
//
//            System.out.println("\nSeptember has 30 days.");
//
//        } else  if (userNum == 10) {
//
//            System.out.println("\nOctober has 31 days.");
//
//        } else  if (userNum == 11) {
//
//            System.out.println("\nNovember has 30 days.");
//
//        } else  if (userNum == 12) {
//
//            System.out.println("\nDecember has 31 days.");
//
//        } else  System.out.printf("\n%d is not a number from 1 to 12", userNum);

//        EXERCISE 5:

//        System.out.println("\nPlease enter three numbers pressing enter after each one: \n");
//
//        double userNum1 = scanner.nextDouble();
//
//        double userNum2 = scanner.nextDouble();
//
//        double userNum3 = scanner.nextDouble();
//
//        if (userNum1 != userNum2 && userNum1 != userNum3 && userNum2 != userNum3) {
//
//            System.out.println("\nAll numbers are different.");
//
//        } else if (userNum1 == userNum2 && userNum1 == userNum3) {
//
//            System.out.println("\nAll numbers are equal.");
//
//        } else System.out.print("\nNeither all are equal or different.");
//
//        System.out.println();


    } // end of main method

} // end of class
