import java.util.Scanner;

public class ControlFlowLesson {

    public static void main(String[] args) {


//        COMPARISON OPERATORS:
//        ==, !=, >, <, >=, <=
//        System.out.println(5 == 2);
//        System.out.println(5 != 2);
//        System.out.println(5 < 2);
//        System.out.println(5 > 2);
//        System.out.println(5 <= 2);
//        System.out.println(5 >= 2);

//        LOGICAL OPERATORS:
//        &&, ||, !
//        System.out.println(5==6 && 2>1 && 3!=3);
//        System.out.println(5!=6 || 2>1 || 3!=3);

//        .equals(), .equalsIgnoreCase()


        Scanner scanner = new Scanner(System.in);
//        System.out.println("Would you like to start? [y/n]");
//        String userInput = scanner.nextLine();


//        DON'T DO THIS:
//        boolean confirm = userInput == "y";

//        DO THIS INSTEAD:
//        boolean confirm = userInput.equals("y");

//        EXAMPLE:

//        if (userInput.equalsIgnoreCase("y")) {
//            System.out.println("Game On!");
//        } else if (userInput.equalsIgnoreCase("n")) {
//            System.out.println("Bye!");
//        } else {
//            System.out.println("It's either y or n!");
//        }




//        IF STATEMENT:

//        char letter = ';'; // remember to use single quotes for char
//
//        if (letter == 'y') {
//            System.out.println("Yes");
//        } else if (letter == 'n') {
//            System.out.println("No");
//        } else {
//            System.out.println("What?");
//        }

//        SWITCH CASE STATEMENT:

//        System.out.println("Enter grade:");
//        String userInput =scanner.nextLine();
//
//        switch (userInput) {
//
//            case "A":
//
//                System.out.println("A");
//                break;
//
//            case "B":
//
//                System.out.println("B");
//                break;
//
//            default:
//
//                System.out.println("Bad grade");
//
//        }

//        WHILE LOOP:

//        int i = 0;
//        while ( i <= 10 ) {
//
//            System.out.println(i);
//            i++;
//        }


//      DO WHILE LOOP:

//        int x = 0;
//
//        do {
//            System.out.println(x);
//            x++;
//        } while (x > 5);

//        FOR LOOP:

//        for (int y = 0; y < 10; y++) {
//
//            System.out.println(y);
//        }


//        BREAK, CONTINUE:
        for (int a = 10; a<= 100; a++) {

            if (a==21) {

//                break;
                continue;
            }

            System.out.println(a);

        }


    }
}
