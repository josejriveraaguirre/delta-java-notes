import java.util.Scanner;

public class Timmy {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String userAns;

        do {

        System.out.println("Meet Timmy. Please try to have a conversation with him by typing simple questions or sentences.");

        String userInput = scanner.nextLine();

        char userTone = userInput.charAt(userInput.length()-1);

        switch (userTone) {

            case '?':

                System.out.println("Sure");
                break;

            case '!':

                System.out.println("Yo, chill bruh");
                break;

        }

        switch (userInput) {

            case "":

                System.out.println("Fine, be that way!");
                break;

            default:

                System.out.println("Yeah... sure");

        }

            System.out.print("\nWould you like to continue? [y/n] ");

            userAns = scanner.next();

        } while (userAns.equalsIgnoreCase("y"));

        System.out.println("\nGoodbye!");

//        or use:

//        if ( userInput.endsWith("?"))

    }
}
