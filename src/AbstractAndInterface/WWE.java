package AbstractAndInterface;
/*
INTERFACE - much like abstract class in that they CANNOT BE INSTANTIATED
instead, they must be implemented by classes or extended by other interfaces
*** contain abstract methods ONLY ***
 */

public interface WWE {

    // abstract methods
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisherMove();

    public abstract void paymentForPerformance(int hours);
}
