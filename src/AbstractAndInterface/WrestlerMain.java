package AbstractAndInterface;

public class WrestlerMain {
    public static void main(String[] args) {
        Wrestler wrestler1 = new RicFlair();

        WWE wrestler2 = new SteveAustin();

        wrestler1.wrestlerName(); // returns Ric Flair
        wrestler1.finisherMove(); // returns Figure Four Leg Lock
        wrestler1.paymentForPerformance(5); // The Wrestler's pay for tonight: 1250


        wrestler2.wrestlerName(); // returns Stone Cold Steve Austin
        wrestler2.finisherMove(); // Stone Cold Stunner
        wrestler2.paymentForPerformance(5); // The WWE Wrestler's pay for tonight: 2500
    }
}

