package AbstractAndInterface;

public class RicFlair extends Wrestler {
    @Override
    public void wrestlerName() {
        System.out.println("Ric Flair");
    }

    @Override
    public void themeMusic() {
        System.out.println("Woooo!");
    }

    @Override
    public void finisherMove() {
        System.out.println("Figure Four Leg Lock");
    }
}
