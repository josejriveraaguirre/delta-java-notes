package AbstractAndInterface;
/*
ABSTRACT CLASS - a special type of class that is created strictly to be a 'base class'
for other classes to derive from
- cannot be instantiated
- may have fields and method just like classes
- they have ABSTRACT METHODS
    - ABSTRACT METHODS - methods that has no body and MUST be implemented in the derived class
                       - only exist in abstract classes
                       - doesn't contain a body

   How to make an abstract class?
   - use the 'abstract' keyword INSIDE OF THE CLASS DEFINITION
 */
public abstract class Wrestler {

    // METHOD
    public static void paymentForPerformance(int hours) {
        System.out.println("The Wrestler's pay for tonight: " + 250*hours);
    }

    // ABSTRACT METHODS
    abstract public void wrestlerName();

    public abstract void themeMusic();

    public abstract void finisherMove();

}
