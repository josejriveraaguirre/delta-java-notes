package Tech;

public class TechMain {

    public static void main(String[] args) {

        Computer comp1 = new Computer("HP", "Desktop", 800);
        Computer comp2 = new Laptop("HP", "Spectre", 1000, 2, 15);
        Computer comp3 = new Smartphone("Apple", "iPhone 12", 1000, 8);
        Laptop comp4 = new Laptop("Dell", "Latitude", 2000, 5, 17);
        Smartphone comp5 = new Smartphone("Apple", "iPhone SE", 500, 4);

        /* TESTING POLYMORPHISM: These don't work.

        Laptop comp6 = new Computer("Dell", "Inspiron", 700);
        Smartphone comp7 = new Computer("Samsung", "Galaxy 10", 900);
        Laptop comp8 = new Smartphone("Samsung", "Galaxy 8", 700, 5);
        Smartphone comp9 = new Laptop("Dell", "XGR", 1200, 10, 17);

         */

        System.out.println(comp1.brand);
        System.out.println(comp1.model);
        System.out.println(comp1.price);

        System.out.println(comp2.brand);
        System.out.println(comp2.model);
        System.out.println(comp2.price);
        System.out.println(((Laptop) comp2).weight);
        System.out.println(((Laptop) comp2).screenSize);

        System.out.println(comp3.brand);
        System.out.println(comp3.model);
        System.out.println(comp3.price);
        System.out.println(((Smartphone) comp3).batteryHours);

        System.out.println(comp4.brand);
        System.out.println(comp4.model);
        System.out.println(comp4.price);
        System.out.println(comp4.weight);
        System.out.println(comp4.screenSize);

        System.out.println(comp5.brand);
        System.out.println(comp5.model);
        System.out.println(comp5.price);
        System.out.println(comp5.batteryHours);


    } //end of psvm

} // end of class
