package Tech;

public class Laptop extends Computer{

    double weight;
    double screenSize;

    public Laptop(String brand, String model, double price, double weight, double screenSize) {
        super(brand, model, price);
        this.weight = weight;
        this.screenSize = screenSize;
    } // end of constructor

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(double screenSize) {
        this.screenSize = screenSize;
    }
} // end of class
