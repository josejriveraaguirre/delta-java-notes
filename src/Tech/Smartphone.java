package Tech;

public class Smartphone extends Computer{

    double batteryHours;

    public Smartphone(String brand, String model, double price, double batteryHours) {
        super(brand, model, price);
        this.batteryHours = batteryHours;
    }

    public double getBatteryHours() {
        return batteryHours;
    }

    public void setBatteryHours(double batteryHours) {
        this.batteryHours = batteryHours;
    }
}
