import java.util.Scanner;

public class ConsoleExtra {

    public static void main(String[] args) {

//        Exercise 1:

//        System.out.println("Hello\nJose");
//
//        Exercise 2:
//
//        int x = 25;
//
//        int y = 5;
//
//        System.out.println(x + " + " + y + " = " + (x + y));

//        or

//        System.out.println(25 + 5);

//        Exercise 3:

          Scanner scanner = new Scanner(System.in);

//        System.out.println("Please enter a whole number:");
//
//        int num1 = scanner.nextInt();
//
//        System.out.println("Please enter another whole number:");
//
//        int num2 = scanner.nextInt();
//
//        System.out.println(num1 + " x " + num2 + " = " + (num1 * num2));

//        or

//        System.out.printf("%d x %d = %d", num1, num2, num1*num2);

//        Exercise 4:

//        System.out.println("Please enter a whole number:");
//
//        int num3 = scanner.nextInt();
//
//        System.out.println("Please enter another whole number:");
//
//        int num4 = scanner.nextInt();

//        System.out.println(num3 + " + " + num4 + " = " + (num3 + num4));

//        or

//        System.out.printf("%d + %d = %d%n", num3, num4, num3+num4);

//        System.out.println(num3 + " - " + num4 + " = " + (num3 - num4));

//        or

//        System.out.printf("%d - %d = %d%n", num3, num4, num3-num4);

//        System.out.println(num3 + " x " + num4 + " = " + (num3 * num4));

//        or

//        System.out.printf("%d x %d = %d%n", num3, num4, num3*num4);

//        System.out.println(num3 + " / " + num4 + " = " + (num3 / num4));

//        or

//        System.out.printf("%d / %d = %d%n", num3, num4, num3/num4);

//        System.out.println(num3 + " % " + num4 + " = " + (num3 % num4));

//        or

//        System.out.printf("%d %% %d = %d%n", num3, num4, num3%num4);

//        Exercise 5:

//        System.out.println("Please enter a string:");
//
//        String userStr = scanner.nextLine();
//
//        System.out.printf("Your string contains %d characters.", userStr.length());

//        or
//
//        int strLen = userStr.length();
//
//        System.out.println("Your string contains " + strLen + " characters.");

//        or

//        int strLen = userStr.length();

//        System.out.printf("Your string contains %d characters.", strLen);

//        Exercise 6:

//        System.out.println("Please enter a number:");
//
//        double num5 = scanner.nextDouble();
//
//        System.out.println("Please enter another number:");
//
//        double num6 = scanner.nextDouble();
//
//        System.out.println("Please enter one more number:");
//
//        double num7 = scanner.nextDouble();
//
//        System.out.printf("The average of " + num5 + ", " + num6 + ", " + num7 + " is: %.2f", ((num5 + num6 + num7)/3));
//
//        or
//
//        System.out.printf("The average of %.2f, %.2f, and %.2f is %.2f", num5, num6, num7, ((num5 + num6 + num7)/3));



    }
}
