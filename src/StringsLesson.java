import java.util.Locale;
import java.util.Scanner;

public class StringsLesson {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String firstName = "Delta";

//        System.out.println("Enter your name: ");
//
//        firstName = scanner.nextLine();
//
//        if (firstName.equals("Delta")) {
//
//            System.out.println("firstName is Delta");
//
//        } else System.out.println("firstName is not Delta");

        System.out.println("Enter your email: ");
        String emailInput = scanner.nextLine();

        emailInput = emailInput.replace(' ', '_');

        System.out.println(emailInput);

        System.out.println(emailInput.length());

        System.out.println("index of @ " + emailInput.indexOf("@"));

        System.out.println("lastIndex of n " + emailInput.lastIndexOf("n"));

        System.out.println("char at " + emailInput.charAt(3));

        for (int x = 0; x < emailInput.length(); x++) {
            System.out.println("email.charAt(x) = " + emailInput.charAt(x));
        }

        if (emailInput.equals("admin@email.com")) {

            System.out.println("You are admin!");

        }

        if (emailInput.equalsIgnoreCase("admin@email.com")) {

            System.out.println("You are admin!");

        }

        if (emailInput.toLowerCase().endsWith("codebound.com")) {

            System.out.println("Welcome back to work!");
        }








    } // end of main method
} // end of class
