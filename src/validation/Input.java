//package validation;
//
//import java.util.Scanner;
//
//public class Input {
//
//    private static Scanner scanner = new Scanner(System.in);
//
//        public static String getString() {
//
////            System.out.print("\nPlease enter a string: ");
//
//            String userInput = scanner.nextLine();
//
//           return userInput;
//        }
//
//    public static boolean yesNo() {
//
//            System.out.print("\nPlease enter yes or no: ");
//
//        String userInput = scanner.nextLine();
//
//        if (userInput.equalsIgnoreCase("yes") || userInput.equalsIgnoreCase("y")) {
//
//            return true;
//
//        } else {
//
//            return false;
//
//        }
//
//    }
//
//        public static boolean yesNo(String userInput) {
//
////            System.out.print("\nPlease enter yes or no: ");
//
//            boolean answer = true;
//
//            userInput = scanner.nextLine();
//
//            if (userInput.equalsIgnoreCase("yes") || userInput.equalsIgnoreCase("y")) {
//
//                answer = true;
//
//            } else if (userInput.equalsIgnoreCase("no") || userInput.equalsIgnoreCase("n")) {
//
//                answer = false;
//
//            }
//            return answer;
//        }
//
//        public static int getInt(int min, int max) {
//
//            int userInput;
//
//            do {
//
//                System.out.printf("%nPlease enter an integer between %d and %d: ", min, max);
//
//                userInput = scanner.nextInt();
//
//            } while (userInput < min || userInput > max);
//
//            return userInput;
//        }
//
//        public static int getInt() {
//
////            System.out.print("\nPlease enter an integer: ");
//
//            int userInput = scanner.nextInt();
//
//            return userInput;
//
//        }
//
//        public static double getDouble(double min, double max) {
//
//            double userInput;
//
//            do {
//
//                System.out.printf("%nPlease enter a number between %.2f and %.2f: ", min, max);
//
//                Scanner scanner = new Scanner(System.in);
//
//                userInput = scanner.nextDouble();
//
//            } while (userInput < min || userInput > max);
//
//            return userInput;
//
//        }
//
//        public static double getDouble() {
//
////            System.out.print("\nPlease enter a double: ");
//
//            double userInput = scanner.nextDouble();
//
//            return userInput;
//
//        }
//
//}

package validation;

import java.util.Scanner;

public class Input {
    private Scanner scanner;

    public Input(Scanner scanner) {
        this.scanner = scanner;
    }

    public Input() {

    }

    public String getString() {
        return scanner.nextLine();
    }

    public boolean yesNo() {
        String userInput = getString();
        return (userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("yes"));
    }

    public int getInt() {
        if (!scanner.hasNextInt()) {
            System.out.println("Not valid integer. Try again");
            scanner.nextLine();
            return getInt();
        }
        else {
            return scanner.nextInt();
        }
    }

    public int getInt(int min, int max) {
        int userInput = getInt();
        if (userInput >= min && userInput <= max) {
            return userInput;
        }
        else {
            System.out.println("Number out of range!");
            return getInt(min, max);
        }
    }

    public double getDouble() {
        if (!scanner.hasNextDouble()) {
            System.out.println("Not valid double. Try again.");
            scanner.nextLine();
            return getDouble();
        }
        else {
            return scanner.nextDouble();
        }
    }

    public double getDouble(double min, double max) {
        double userDouble = getDouble();
        if (userDouble >= min && userDouble <= max) {
            return userDouble;
        }
        else {
            System.out.println("Number out of range!");
            return getDouble(min, max);
        }
    }


}

