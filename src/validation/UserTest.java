//package validation;
//
//import static validation.Input.*;
//
//public class UserTest {
//
//    public static void main(String[] args) {
//
//        System.out.print("\nPlease enter a string: ");
//
//        System.out.println(getString());
//
//        System.out.print("\nPlease enter yes or no: ");
//
//        System.out.println(yesNo());
//
//        System.out.print("\nPlease enter an integer: ");
//
//        System.out.println(getInt());
//
//        System.out.print("\nPlease enter a double: ");
//
//        System.out.println(getDouble());
//
//    }
//
//
//}

package validation;

import java.util.Scanner;

public class UserTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Input input = new Input(scanner);

//        System.out.println("Enter your favorite color: ");
//        String favColor = input.getString();
//        System.out.println(favColor);
//
//        System.out.println("Are you awesome? [y/n]");
//        boolean userIsAwesome = input.yesNo();
//
//        if (userIsAwesome) {
//            System.out.println("You are really awesome!");
//        }
//        else {
//            System.out.println("Believe in yourself");
//        }

        System.out.println("Please enter a number between 1 and 3");
        int userInt = input.getInt(1, 3);
        System.out.println("User input: " + userInt);

        System.out.println("Please enter a number between 1 and 3");
        double userDouble = input.getDouble(1, 3);
        System.out.println("User input: " + userDouble);
    }
}
