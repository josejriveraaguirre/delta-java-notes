public class Student {

//    fields

//    access modifiers - public/private

    public String name;
    public String cohort;

//    private instance variable

    private double grade;

//    constructor - needs to have the same name as the class

    public Student(String studentName, String assignedCohort) {

        name = studentName;

        cohort = assignedCohort;

    }

    public Student(String studentName) {

        name = studentName;

        cohort = "Unassigned to a cohort";

    }

//   The  "this." keyword provides  a way to refer to the current instance. It's the preferred method.

    public Student(String name, String cohort, double grade) {

        this.name = name;

        this.cohort = cohort;

        this.grade = grade;

    }

    public String getStudentInfo() {

        return String.format("Student Name: %s%nCohort Assigned: %s%n",name,cohort);

    }

    public String sayHello() {

        return "Hello I am " + this.name;

    }

    public double shareGrade() {

        return grade;

    }
}
