import java.util.Arrays;
import java.util.Scanner;

public class StringMethodsExercise {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

//        int myLength = "Good afternoon, good evening, and good night";

        String myLength = "Good afternoon, good evening, and good night!";

        System.out.println(myLength.length());

        String uCase = "Good afternoon, good evening, and good night!";

        System.out.println(uCase.toUpperCase());

        String lCase = "Good afternoon, good evening, and good night!";

        System.out.println(lCase.toLowerCase());

        String firstSubstring6 = "Hello World".substring(6);

        System.out.println(firstSubstring6);

        String firstSubstring3 = "Hello World".substring(3);

        System.out.println(firstSubstring3);

        String firstSubstring10 = "Hello World".substring(10);

        System.out.println(firstSubstring10);

        String message = "Good evening, how are you?";

        System.out.println(message.substring(0,12));

        System.out.println(message.substring(14));

//        char myChar = "San Antonio";

        String myChar = "San Antonio";

        System.out.println(myChar.charAt(0));

        System.out.println(myChar.charAt(4));

        System.out.println(myChar.charAt(myChar.indexOf("o")));

        String alpha = "Alyssa Angela Jose Victor";

        String[] splitAlpha = alpha.split(" ");

        System.out.println(Arrays.toString(splitAlpha));

        String m1 = "Hello, ";
        String m2 = "how are you?";
        String m3 = "I love Java!";

        System.out.println(m1 + m2 + " " + m3);

        int result = 89;

        System.out.println("You scored " + result + " marks for your test!");

    } // end of Main method
} // end of class
