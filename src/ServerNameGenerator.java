import java.util.Random;

public class ServerNameGenerator {

    public static String[] ADJECTIVES = {"Adroit", "Bellicose", "Contumacious", "Didactic", "Effulgent",
            "Fecund", "Garrulous", "Heuristic", "Inveterate", "Jocular"};

    public static String[] NOUNS = {"Ava", "Bryson", "Catalina", "Dempsey", "Elina",
            "Fenton", "Gabriela", "Holden", "Indigo", "Joaquin"};

    public static String randomAdj () {

        Random random = new Random();

        return ADJECTIVES[random.nextInt(ADJECTIVES.length)];
    }

    public static String randomNoun () {

        Random random = new Random();

        return NOUNS[random.nextInt(NOUNS.length)];
    }

    public static void main(String[] args) {

        System.out.println("\n" + randomAdj() + "-" + randomNoun());

    } // end of psvm

} // end of class

// After class, I will try to do it with one method instead of two, similar to Stephen's.
