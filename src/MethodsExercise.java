import java.util.Scanner;

public class MethodsExercise<leap> {

    public static void main(String[] args) throws InterruptedException {

//        int num1 = 5;
//        int num2 = 3;
//
//        double num3 = 4;
//        double num4 = 7;
//
//        System.out.println(addNumbers(num1,num2));
//
//        System.out.println(substractNumbers(num1,num2));
//
//        System.out.println(multiplyNumbers(num1,num2));
//
//        System.out.println(divideNumbers(num3,num4));
//
//        System.out.println(modulusNumbers(num3,num4));
//
//        getInteger();

//        getInteger1(1,10);
//
//        calculateFactorial();
//
//        calculateAverage();

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.print("\nPlease enter a year (yyyy): ");
//
//        int year = scanner.nextInt();
//
//        boolean leap = isLeapYear(year);
//
//        System.out.printf("%d is a leap year: %b", year, leap);

//        MY LITTLE LEAP YEAR GAME:

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.print("\nPlease enter a year (yyyy): ");
//
//        int year = scanner.nextInt();

//        System.out.printf("\nTrue or False: %d is a leap year.(You have 5 seconds to think your answer.)\n", year);
//
//        System.out.printf("\nThe correct answer is: %b.\n", isLeapYear(year));
//
//        System.out.println("\nI hope you got it right. Bye!");

    } // end of psvm


//    METHODS:

    public static int addNumbers(int num1, int num2) {

        return num1 + num2;

    }

    public static int substractNumbers(int num1, int num2) {

        return num1 - num2;

    }

    public static int multiplyNumbers(int num1, int num2) {

        return num1 * num2;

    }

    public static double divideNumbers(double num1, double num2) {

        double result = 0;

        if (num2 == 0) {

            System.out.println("Division by zero!");

        } else {

            result =  num1 / num2;

        }

        return result;

        }

    public static double modulusNumbers(double num1, double num2) {

        double result = 0;

        if (num2 != 0) {

            result =  num1 % num2;


        } else {

            System.out.println("Division by zero!");

        }

        return result;

    }

    public static void getInteger() {

        Scanner scanner = new Scanner(System.in);

        int userInput;

        do {

            System.out.print("\nPlease enter a number between 1 and 10: ");

             userInput = scanner.nextInt();

        } while (userInput < 1 || userInput > 10);

        System.out.printf("\nYou entered %d.\n", userInput);

    }

//    or

    public static int getInteger1(int min, int max) {

        Scanner scanner = new Scanner((System.in));

        System.out.printf("Enter a valid number: (%d - %d)%n", min, max);

        int userInput = scanner.nextInt();

        if (userInput >= min && userInput <= max) {

            System.out.println("Valid number!");

            System.out.println("You entered: " + userInput);

            return userInput;

        } else {

            System.out.println("Invalid entry: Number must be between " + min + "-" + max);

            return getInteger1(min,max);
        }

    }

    public static void calculateFactorial() {

        Scanner scanner = new Scanner(System.in);

        int userInput;

        do {

            System.out.print("\nPlease enter a number between 1 and 10: ");

            userInput = scanner.nextInt();

        } while (userInput < 1 || userInput > 10);

                int result = 1;

                for (int i = 1; i <= userInput; i++){

                    result *= i;

                }

                System.out.printf("\nThe factorial of %d is %d.\n", userInput, result);

    }

    public static void calculateAverage() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("\nPlease enter a number: ");

        double num5 = scanner.nextDouble();

        System.out.print("\nPlease enter another number: ");

        double num6 = scanner.nextDouble();

        System.out.print("\nPlease enter one more number: ");

        double num7 = scanner.nextDouble();

        System.out.printf("%nThe average value is %.1f%n", ((num5 + num6 + num7) / 3));
    }

    public static boolean isLeapYear(int year) {

        boolean leap = false;

        if (year < 1 || year > 9999) {

            leap = false;

        } else if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {

            leap = true;

        }

        return leap;

    }





//    public static boolean isLeapYear (int year) throws InterruptedException {
//
//        boolean leap;
//
//        if (year < 1 || year > 9999) {
//
//            leap = false;
//
//              } else if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
//
//            leap = true;
//
//        }
//
//        Thread.sleep(10000);
//
//     return leap;
//
//    }

} // end of class
