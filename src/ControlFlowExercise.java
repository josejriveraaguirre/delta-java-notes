import java.util.Scanner;

public class ControlFlowExercise {

    public static void main(String[] args) {


//        LOOP BASICS:

//        WHILE LOOP:

//        int i = 9;
//
//        while (i <= 23) {
//
//            System.out.printf("%d ", i);

//            i++;
//        }

//        DO WHILE:

//        int i = 0;
//
//        do {
//            System.out.println(i);
//            i+=2;
//        } while (i <= 100);

//        int i = 100;
//
//        do {
//            System.out.println(i);
//            i-=5;
//        } while (i >= -10);

//        DO WHILE EXPONENTS WIP:

//        long i = 2;
//
//        do {
//            System.out.println(i);
//
//            i *= i;
//
//        } while (i <= 1000000);
//
//        or
//
//        int i = 2;
//
//        do {
//            System.out.println(i);
//
//            i = (int)Math.pow(i,2);
//
//        } while (i <= 1000000);


//  TABLE OF POWERS WIP:

        Scanner scanner = new Scanner(System.in);

//        String userAns;
//
//        do {
//
//            System.out.print("\nWhat number would you like to go up to? ");
//
//            int userNum = scanner.nextInt();
//
//            System.out.println("\nHere's your table!");
//
//            System.out.println("\n Number  |  Squared |  Cubed  ");
//
//            System.out.println("======== | ======== | ========");
//
//            for (int i = 1; i <= userNum; i++) {
//
//                int userSqrd = (int) Math.pow(i, 2);
//
//                int userCubd = (int) Math.pow(i, 3);
//
//                System.out.printf("%4d     | %5d    | %5d    \n", i, userSqrd, userCubd);
//
//            }
//
//            System.out.print("\nWould you like to continue? [y/n] ");
//
//            userAns = scanner.next();
//
//        } while (userAns.equalsIgnoreCase("y"));
//
//        System.out.println("\nGoodbye!");


//        NUMBER GRADES TO LETTER GRADES:

        String userAns;

        do {

        System.out.print("\nPlease enter your number grade [0-100] ");

        int userNum = scanner.nextInt();

            System.out.println();

        if (userNum <= 59) {

                System.out.println("Your Letter Grade is : F");

        } else if (userNum >= 60 && userNum<= 69){

            System.out.println("Your Letter Grade is : D");

        } else if (userNum >= 70 && userNum<= 79) {

            System.out.println("Your Letter Grade is : C");

        } else if (userNum >= 80 && userNum<= 89) {

            System.out.println("Your Letter Grade is : B");

        } else if (userNum >= 90 && userNum<= 100) {

            System.out.println("Your Letter Grade is : A");

        } else {

            System.out.println("Invalid number grade.");

        }

        System.out.print("\nWould you like to continue? [y/n] ");

        userAns = scanner.next();

        } while (userAns.equalsIgnoreCase("y"));

         System.out.println("\nGoodbye!");


    } // end of main method
} // end of class
